/* global TimelineMax, Power4, EB, EBG */

// Broadcast Events shim
// ====================================================================================================
(function() {
    if (typeof window.CustomEvent === 'function') { return false; }

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
})();

// Timeline
// ====================================================================================================
var timeline = (function MasterTimeline() {

    var tl;
    var win = window;

    function doClickTag() { window.open(window.clickTag); }

    function initTimeline() {
        document.querySelector('#ad .banner').style.display = 'block';
        document.getElementById('ad').addEventListener('click', doClickTag);
        createTimeline();
    }

    function createTimeline() {
        tl = new TimelineMax({delay: 0.25, onStart: updateStart, onComplete: updateComplete, onUpdate: updateStats});
        // ---------------------------------------------------------------------------


        tl.add('frame1')
        .from('.hl1', .75, {x:"-50%", opacity: 0, ease: Power3.easeOut }, 'frame1+=.25')
        .to('.hl1', .75, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=1.5')

        .from('.hl2', .75, {x:"-50%", opacity: 0, ease: Power3.easeOut }, 'frame1+=1.75')
        .from('.hl2b', .75, {x:"-50%", opacity: 0, ease: Power3.easeOut }, 'frame1+=1.75')

        .to('.hl2b', .75, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=3')

        .from('.hl3', .75, {x:"-50%", opacity: 0, ease: Power3.easeOut }, 'frame1+=3.25')
        .to('.hl3', .75, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=4.5')

        .from('.hl4', .75, {x:"-50%", opacity: 0, ease: Power3.easeOut }, 'frame1+=4.75')
        .to('.hl4', .75, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=6')
        .to('.hl2', .75, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=6')

        .from('.hl5', .75, {x:"-50%", opacity: 0, ease: Power3.easeOut }, 'frame1+=6.25')
        .to('.hl5', .75, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=7.5')

        .from('.hl6', .75, {x:"-50%", opacity: 0, ease: Power3.easeOut }, 'frame1+=7.75')
        .to('.hl6', .75, {opacity: 0, ease: Power3.easeInOut }, 'frame1+=9')

        tl.add('frame2')

        .from('.background2', 1.5, {x:"100%", ease: Power3.easeOut }, 'frame2')
        .from('.whiteblock', .75, { opacity: 0, ease: Power3.easeOut }, 'frame2')
        .from('.logo', 1, { opacity: 0, ease: Power3.easeOut }, 'frame2+=1')
        .from('.hl7', 1, { opacity: 0, ease: Power3.easeOut }, 'frame2+=1.25')
        .from('.tri', 1.25, { opacity: 0, ease: Power3.easeOut }, 'frame2+=1.75')
        .from('.disc', 1.25, { opacity: 0, ease: Power3.easeOut }, 'frame2+=2.25')


        // ---------------------------------------------------------------------------

        // DEBUG:
        // tl.play('frame3'); // start playing at label:frame3
        // tl.pause('frame3'); // pause the timeline at label:frame3
    }

    function updateStart() {
        var start = new CustomEvent('start', {
            'detail': { 'hasStarted': true }
        });
        win.dispatchEvent(start);
    }

    function updateComplete() {
        var complete = new CustomEvent('complete', {
            'detail': { 'hasStopped': true }
        });
        win.dispatchEvent(complete);
    }

    function updateStats() {
        var statistics = new CustomEvent('stats', {
            'detail': { 'totalTime': tl.totalTime(), 'totalProgress': tl.totalProgress(), 'totalDuration': tl.totalDuration()
            }
        });
        win.dispatchEvent(statistics);
    }

    function getTimeline() {
        return tl;
    }

    return {
        init: initTimeline,
        get: getTimeline
    };

})();

// Banner Init
// ====================================================================================================
timeline.init();
